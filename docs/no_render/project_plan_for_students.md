---
title: '21S ITT2 Project'
subtitle: 'Project plan, part I'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Mathias Gregersen \<megr@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: \today
email: 'nisi@ucl.dk'
left-header: \today
right-header: Project plan
skip-toc: false
skip-tof: true
---

# Background

This is the semester ITT2 project where you will work with different projects, initated by a company. This project plan will cover the project, and will match topics that are taught in parallel classes.

The overall project case is described at [https://datacenter2.gitlab.io/datacenter-web/](https://datacenter2.gitlab.io/datacenter-web/)

# Purpose

The main goal is to have a system where IoT sensors collect data from a datacenter.
This is a leaning project and the primary objective is for the students to get a good understanding of the challenges involved in making such a system and to give them hands-on experience with the implementation.

For simplicity, the goals stated wil evolve around the technical project goals. It must be read using the weekly plan as a complementary source for the *learning* oriented goals.

# Goals

The overall system that is going to be build looks as follows:  
![project_overview](../docs/datacenter_iot_system_overview.png "ITT2 project overview")  
Reading from the left to the right:  

* Sensor modules 1-3: A placeholder for x number of sensors
* Raspberry Pi: The embedded system to run the sensor software and to be the interface to the MQTT broker.  
* Computer: Connects to both the Raspberry Pi and Gitlab while developing and troubleshooting  
* MQTT broker: MQTT allows for messaging between device to cloud and cloud to device. This makes for easy broadcasting messages to groups of things. 
* Consumer: 


Project deliveries are:  

* A system reading and writing data to and from the sensors/actuators  
* Docmentation of all parts of the solution  
* Regular meeting with project stakeholders.  
* Final evaluation 
* TBD: Fill in more, as agreed, with teams from the other educations


# Schedule

See the [lecture plan](https://eal-itt.gitlab.io/21s-itt2-project/other-docs/21S_ITT2_PROJECT_lecture_plan.html) for details.

# Organization

[Considerations about the organization of the project may includes  
    • Identification of the steering committee  
    • Identification of project manager(s)  
    • Composition of the project group(s)  
    • Identification of external resource persons or groups  

For each of the identified groups and people, their tasks must be specified and their role in the project must be clear.  This could be done in relation to the goals of the project.]

TBD: This section must be completed by the students.


# Budget and resources

Small monetary resources are expected. In terms of manpower, only the people in the project group are expected to contrbute,


# Risk assessment

[The risks involved in the project is evaluated, and the major issues will be listed.  
The plan will include the actions taken as part of the project design to handle the risk or the actions planned should a given risk materialize.]

TBD: This section must be completed by the students based on the pre-mortem meeting


# Stakeholders

[An analysis of the stakeholder is conducted to establish who has an interest in the project. There will be multiple stakeholders with diverse interests in the project.

Possible stakeholders  
    • Internal vs. external  
    • Positive vs. negative  
    • Active vs. passive  

A strategy could be planned on how to handle each stakeholder and how to handle stakeholders different (and/or conflicting) interests and priorities.  
This could also include actions designed to transform a person or a group into a (positive) stakeholder, or increase the value of the project for a given stakeholder.]

TBD: This section is to be completed by the students.  
TBD: Insert your relevant external stakeholder.

# Communication

[The stakeholders may have some requirements or requests as to what reports or other output is published or used. Some part of the project may be confidential.  
Whenever there is a stakeholder, there is a need for communication. It will depend on the stakeholder and their role in the project, how and how often communication is needed.  
In this section, reports, periodic emails, meetings, facebook groups and any other stakeholder communication will be described]  

TBD: Students fill out this part based on risk assessment and stakeholders.

# Perspectives

This project will serve as a template for other similar projects in future semesters.

It can also serve as an example of the students abilities in their portfolio, when applying for jobs or internships.


# Evaluation

[Evaluation is about how to gauge is the project was successful. This includes both the process and the end result. Both of which may have consequences on future projects.  
The will be some documentation needed at the end (or during) the project, e.g. external funding will require some sort of reporting after the project has finished. This must be described.]  

TBD: Students fill out this one also

# References

[Include any relevant references to legal documents, literature, books, home pages or other material that is relevant for the reader.]

TBD: Students may add stuff here at their discretion


* Common project group on gitlab.com

  The project is managed using gitlab.com. The groups is at [https://gitlab.com/20s-itt-dm-project](https://gitlab.com/20s-itt-dm-project)
