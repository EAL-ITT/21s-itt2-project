---
title: '21S ITT2 Project'
subtitle: 'Lecture plan'
filename: '21S_ITT2_PROJECT_lecture_plan'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Mathias Gregersen \<megr@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
email: 'nisi@ucl.dk'
left-header: \today
right-header: Lecture plan
skip-toc: false
semester: 21S
---


# Lecture plan

The lecture plan consists of the following parts: a week/lecture plan, an overview of the distribution of study activities and general information about the course, module or project.

* Study program, class and semester: IT technology, oeait20ei, 21S
* Name of lecturer and date of filling in form: NISI, MGRE 2020-12-13
* Title of the course, module or project and ECTS: Semester project, 12 ECTS
* Required readings, literature or technical instructions and other background material: None

See weekly plan for details like detailed daily plan, links, references, exercises and so on.


| Teacher |  Week | Content |
| :---: | :---: | :--- |
| NISI/MGRE | 05 | Introduction, tools and online resources: scrum, sprint, system overview, gitlab/project setup, project plan, expectations, s.m.a.r.t tasks, project conclusion, Team roller revisited |
| NISI/MGRE | 06 | Kickoff day (OME, IT technology, Industry), System brainstorm, analysis and requirements draft. MQTT introduction, Research OME education |
| NISI/MGRE | 07 | Sensor principles, sensor key parameters, sensor suppliers, sensor data via MQTT |
| NISI/MGRE | 08 | Cloud, MQTT broker cloud microservice, Team roles rotation plan, Milestones |
| NISI/MGRE | 09 | Data persistence cloud microservice, document database |
| NISI/MGRE | 10 | Milestone: Proof of concept. Dashboard cloud microservice, json data, POC video presentations |
| NISI/MGRE | 11 | Project cleanup, Minimum viable product start, internship conference |
| NISI/MGRE | 12 | Requirements, Node-red, Sensor purchase |
| NISI/MGRE | 13 | Easter holiday |
| NISI/MGRE | 14 | Easter monday, Gitlab Groups, Git branching |
| NISI/MGRE | 15 | Milestone: Minimum Viable Product.  Node red international workshop, Documentation update  |
| NISI/MGRE | 16 | MVP video presentation, MVP User test |
| NISI/MGRE | 17 | System robustness, MQTT security |
| NISI/MGRE | 18 | Custom RPi images, Business understanding |
| NISI/MGRE | 19 | Project website, Signal processing  |
| NISI/MGRE | 20 | Milestone: Consolidation. Unit testing, System test plan |
| NISI/MGRE | 21 | Presentation prep, Project presentations and demo |
| NISI/MGRE | 22 | Project wrapup, Report handin |
|  NISI/MGRE | 23 | Gitlab project finishing touches |

# General info about the course, module or project


## The student’s learning outcome

Learning goals for the course are described in the curriculum section 2 about the national elements.

Besides consolidating the material from the other courses, the project is directly related to the topic about project management (section 2.4 in the curriculum).
The project is a collaboration between different educations and the danish datacenter industry. The project specific learning goals combined with the curriculum learning goals are stated below:

## Knowledge 

**The student possesses knowledge and understanding of**  

* What innovation is and how innovative methods are used in problem solving
project management of IT development projects
* How a business is organized, including which sections direct the business and how its economy can be described overall
* Quality and resource management as part of a development project and as part of the management of the maintenance of IT operations
* The advice and consultancy function, when an IT specialist is called upon to understand and meet customer requirements
* Other professional groups' practices in relation to the data center industry
* The value-creating interaction between disciplines in relation to the data center industry
* Development-based knowledge of the industry's applied practice, theory and methodology within technology development (ex Signal processing)
* Basic business understanding in relation to the data center industry

## Skills 

**The student is able to**  

* Communicate orally and in writing with both professionals and customers
* Communicate key results
* Evaluate issues and develop prototype solutions in relation to the chosen topic(s) 

## Competencies

**The student is able to**

* In a structured setting, acquire skills and new competencies in the understanding of businesses and customers’ use of IT
* Manage customer assignments in order to translate customer needs into secure solutions
* Undertake planning, manage the student’s own technical tasks and take part in projects
* Participate in academic and interdisciplinary collaboration and project work in connection with technology development with a professional approach, and participate in the development of practice within technology development
* Participate in practical development processes


## Content

The course is formed around a project, and will include relevant technologies from the curriculum. It is intended for cross-topic consolidation.

The project case is described at [https://datacenter2.gitlab.io/datacenter-web/](https://datacenter2.gitlab.io/datacenter-web/)

## Method

The project is divided into milestones.  

Students will in collaboration with other professions decide on requirements they can work towards during the project.

The idea is to create a system that works from sensor to presentation while the students practice project management, communication and learn operational skills.

## Equipment

* Raspberry Pi
* Computer

Students might need to use the equipment from other courses.  
Students might need to buy additional equipment for the course. 

## Projects with external collaborators 


[Operational maintenance engineering education](http://www.fms.dk/om-uddannelsen/)   
[Danish datacenter industry](https://datacenterindustrien.dk/)   


## Test form/assessment
The project includes 1 compulsory element.

See semester description on [https://www.ucl.dk/international/full-degree/it-technology](https://www.ucl.dk/international/full-degree/it-technology) for details on compulsory elements.

## Other general information
None at this time.
