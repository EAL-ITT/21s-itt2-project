---
Week: 17
tags:
- Robustness
- Security
- MQTT Security
---

# Exercises for ww17

## Exercise 1 - System robustness

### Information

Robustness in systems is in the context of this exercise about making your sensor system (RPi + sensor(s) ) robust.
A number of things can happen to a sensor system in the wild.

* Loss of power to the RPi
* Loss of internet connection
* Faulty sensor
* Application error

In any of the above scenarios you have to consider the consequences to your entire system. 
Often you want to make sure that data (sensor readings) are not lost or the device is able to restart after a power loss. 

This exercise requires you to look into methods of starting your python application on RPi boot and also implementing one or more MQTT topic for error notifications. 

*It is recommended to put any configuration of the RPi into shell/bash scripts for backup and reproduction purposes*

### Exercise instructions

Start by examining the scenarios and how you can detect them from your MQTT python client application.  
Note down your findings and plan how to implement the below.  


For each of the 4 above scenarios implement:

  *  Restart of python application after power loss + notification on dashboard when the sensor system is running again. 
  *  Visual feedback with an LED on loss of internet connection + connection status on dashboard
  * Visual feedback with an LED on faulty sensor + notification on dashboard 
  * Notification on dashboard if the python application is not running

**Bonus exercise**  

On loss of internet connection, implement local storage on the RPi, of up to 10 minutes of readings. Send them to the broker when the connection is re-established and delete the local storage after the local stored readings have been sent to the broker.  
Remember to timestamp your readings to keep the order.  

**Exercise documentation**

Conclude the exercise with a small manual describing the behaviour on both the device and the dashboard for each of the scenarios you have implemented.  
Link to the manual from your `readme.md` file

**Links**

* Handle MQTT disconnection to broker (found by yozi2044) [https://mntolia.com/mqtt-last-will-testament-explained-with-examples/](https://mntolia.com/mqtt-last-will-testament-explained-with-examples/)

## Exercise 2 - MQTT security

### Information

This is a team exercise. This exercise will have you look at the security aspect of using mqtt. You will have to identify and asses potential vulnerabilities in your solution. You will also get to look at how to potentially fix some of the vulnerabilities you have found.

### Exercise instructions

**Part 0 - Setup and preparation**

1. Create a document on gitlab for this exercise and link to it from your `readme.md`
2. Read the following article: [https://www.iotforall.com/mqtt-security-three-basic-concepts](https://www.iotforall.com/mqtt-security-three-basic-concepts)  

**Part 1 - Identify security vulnerabilities**

1. Create a list of potential vulnerabilities for your system.
2. For each vulnerability, create a small description of the consequence(s) of the vulnerability being exploited.
3. Give each vulnerability a threat level based on the potential consequence(s) when exploited.

In the following articles, you can find inspiration on vulnerabilities in mqtt:  
[https://blog.paessler.com/why-mqtt-is-everywhere-and-the-security-issues-it-faces](https://blog.paessler.com/why-mqtt-is-everywhere-and-the-security-issues-it-faces)  
[https://blog.avast.com/mqtt-vulnerabilities-hacking-smart-homes](https://blog.avast.com/mqtt-vulnerabilities-hacking-smart-homes)

**Part 2 - Suggest solutions to the security vulnerabilities**

1. For each vulnerability, create one or more potential solutions to the vulnerability
2. Based on feasability of the potential solutions and the threat levels of the vulnerabilities, select some of the solutions for implementation and make a description of how and when thay will be implemented.

You can learn more about the security solutions already part of mqtt on the following page: [https://www.hivemq.com/mqtt-security-fundamentals/](https://www.hivemq.com/mqtt-security-fundamentals/)  

**Part 3 - Implement the solutions**

Spend some time implementing the agreed upon solutions.