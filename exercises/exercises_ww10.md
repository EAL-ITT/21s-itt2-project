---
Week: 10
tags:
- node-red
- dashboard
- milestone
- proof of concept
- cloud
---

# Exercises for ww10

## Exercise 1 - node-red dashboard on Azure

For learning purposes this is recommended as an individual exercise, for your team's system, you most likely only need one node-red application. 

### Information

Visualization of data in an IoT system can be a large task. Using node-red to create a dashboard for an IoT system simplifies the task while still providing a lot of flexibility.

As defined by the creators, "Node-RED is a programming tool for wiring together hardware devices, APIs and online services in new and interesting ways. It provides a browser-based editor that makes it easy to wire together flows using the wide range of nodes in the palette that can be deployed to its runtime in a single-click."

In this exercise you will learn how to setup a node-red application on azure and how to create a simple dashboard for users to interface with the IoT system.

### Exercise instructions

**Part 1 - Setup node-red on azure**

Use the following guide to setup a vm on azure and installing node-red on the vm. Use the OS image named `Debian 10 "Buster" with backports kernel - Gen1` and a machine size named `Standard B1s (1 vcpus, 1 GiB memory)` instead of the one suggested in the guide: [https://nodered.org/docs/getting-started/azure](https://nodered.org/docs/getting-started/azure)

**Part 2 - Install node-red-dashboard nodes**

1. Go to the node-dashboard using your preferred browser. The address should be something like `<Your-node-red-VM-IP>:1880` 
2. Click the top right menubar (denoted by the three horizontal lines on top of each other)
3. Click manage palette
4. Click on the install tab
5. Search for `node-red-dashboard`
6. Install `node-red-dashboard` from the search results (should be the top result)
7. Check that you have a new tab of nodes called dashboard

**Part 3 - Install node-red-node-mongodb nodes**

1. Follow step 1 through to 4 from part 2
2. Search for `node-red-node-mongodb`
3. Install `node-red-node-mongodb` from the search results (make sure you install the correct one)
4. Check that you have new nodes under the storage tab called `mongodb in` and `mongodb out`

**Part 4 - Create a simple dashboard**

The following video guide can be useful as an intro into making dashboards in node-red. It is advised to watch it before starting this part of the exercise: [https://www.youtube.com/watch?v=X8ustpkAJ-U&ab_channel=FreeWaveTechnologies](https://www.youtube.com/watch?v=X8ustpkAJ-U&ab_channel=FreeWaveTechnologies)  

1. Insert a `mqtt in` node
2. Set the `mqtt in` node to connect to your azure mqtt broker
3. Set the `mqtt in` node to subscribe to a topic for testing
4. Set the output of the `mqtt in` node to be a parsed JSON object
5. Insert a `change` node and connect its input to the out put of the `mqtt in` node
6. Add a rule setting `msg.timestamp` to `msg.payload.<your timestamp key>`
7. Add a rule setting `msg.payload` to `msg.payload.<your sensor value key>`
8. Insert a `chart` node and connect its input to the `change` node
9. Create a new ui group and tab
10. Set the X-axis `chart` node to the last 1 minutes
11. Set the Y-axis to expected min and max values
12. Deploy the solution
13. Confirm the solution was deploy by going to `<Your-node-red-VM-IP>:1880/ui` and confirming an empty graph is present

**Part 5 - Testing the dashboard**

1. Start your mqtt-broker
2. Make sure the `mqtt in` node is connected to the broker (small green square underneath saying connected)
3. Publish data to the topic using your test publisher from the previous weeks
4. Go to `<Your-node-red-VM-IP>:1880/ui` and confirm your mqtt data is getting displayed in the graph

**Part 6 - Adding MongoDB (bonus exercise)**

Add a new chart to your node-red solution and make it display some of the sensor data contained in your MongoDB database. In order to do this you should take a closer look at the `mongodb in` node.

## Exercise 2 - POC milestone video presentation

### Information

Proof of concept (POC) ends in week 10 and we would like to see what you have accomplished.

You need to produce a video ~3 minutes long, that includes a walkthrough and demo of your POC system, from sensor to dashboard. The video should be uploaded to youtube or similar and shared with teachers and the other project groups.

## Instructions

1. Setup your POC system
2. Make a video that explains the moving parts (RPi, VM's dashboard etc.) and a small demo of the system
3. Share your video by putting a link to it, in your `README.md` file

The deadline for submission of the video is 2021-03-10 16:15 AM.
