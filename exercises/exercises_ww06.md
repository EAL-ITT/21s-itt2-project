---
Week: 06
tags:
- system analysis
- requirements
- scrum
- mqtt
- research
---

# Exercises for ww06

## Exercise 1 - System brainstorm, analysis and requirements draft

### Information

When you start development of a new system it is important that you take the time to specify the requirements.

This can be done in many ways, this exercise gives you some guidelines on how to get an overview of what you would like to build.

The requirements can change over time as you gain knowledge during the project, because of this the output from this exercise is considered a draft of the requirements.

### Exercise instructions

**Part 1 - System brainstorm**

1. Read about the definition of a brainstorm [https://en.wikipedia.org/wiki/Brainstorming](https://en.wikipedia.org/wiki/Brainstorming)
2. Discuss your understanding of a brainstorm in the team
3. Choose a brainstorm method variation of Osborn's method:
    * Nominal group technique
    * Group passing technique
    * Team idea mapping method
    * Directed brainstorming
    * Guided brainstorming
    * Individual brainstorming
    * Question brainstorming
4. Do a brainstorm from the question "what system will we be building ?"
    * Create a document that captures the output of your brainstorm, this can be a simple google document or a mind map using [https://app.diagrams.net/](https://app.diagrams.net/) which is more intuitive in a brain storm process.  
    Everyone in the team has to be able to edit the document during the process
5. Document the brainstorm in your gitlab project and link to it in the `readme.md` file

**Part 2 - System analysis**

1. From your brainstorm create a block diagram showing an overview of your system blocks
2. Read about use case and use case diagrams [https://www.visual-paradigm.com/guide/uml-unified-modeling-language/what-is-use-case-diagram/](https://www.visual-paradigm.com/guide/uml-unified-modeling-language/what-is-use-case-diagram/)
3. In you team discuss your understanding of use cases and how they can be used as a communication tool with the OME's
4. In your team dicuss and describe several "Use cases" for your system using use case diagrams.  
[https://app.diagrams.net/](https://app.diagrams.net/) has the nessecary use case diagram objects
5. Document the use cases and diagrams in your gitlab project and link to it in the `readme.md` file

**Part 3 - requirements draft**

1. From your block diagram and use cases condence a list of requirements. Requirements are both issues you as a team need to do and technical requirements like components, protocols, coding language and environment.  
In short, everything that you can see as requirements at this point in time.  
2. From your list of requirements create issues in your product backlog, remember your knowledge about s.m.a.r.t tasks and especially if your issues is too big!
3. Document the requirements list in your gitlab project and link to it from the `readme.md` file

## Exercise 2 - MQTT introduction

### Information
This exercise will help you get started with using MQTT. You will be using a python client library called paho-mqtt.  
This exercise should be performed on your raspberry pi.

Throughout the exercise reguarly commit your code to your gitlab project.  
When the exercise is completed link to your code in the `readme.md` file 

### Exercise instructions

**Part 1 - The publish/subscribe pattern**  

Understanding the publish/subscribe pattern is important before starting using MQTT.  
Spend some time getting a good understanding of the publish/subscribe pattern in relation to MQTT.  
You can use the following resources:  
- [https://www.hivemq.com/blog/mqtt-essentials-part-1-introducing-mqtt/](https://www.hivemq.com/blog/mqtt-essentials-part-1-introducing-mqtt/)
- [https://www.hivemq.com/blog/mqtt-essentials-part2-publish-subscribe/](https://www.hivemq.com/blog/mqtt-essentials-part2-publish-subscribe/)
- [https://www.youtube.com/watch?v=jTeJxQFD8Ak&list=PLRkdoPznE1EMXLW6XoYLGd4uUaB6wB0wd&ab_channel=HiveMQ](https://www.youtube.com/watch?v=jTeJxQFD8Ak&list=PLRkdoPznE1EMXLW6XoYLGd4uUaB6wB0wd&ab_channel=HiveMQ)

**Part 2 - Installing paho-mqqt**  

Install the paho-mqtt python library using pip: [https://pypi.org/project/paho-mqtt/](https://pypi.org/project/paho-mqtt/)  

**Part 3 - Creating a publisher**  

Create a publisher using the paho-mqtt library. Some guidance on how can be found on the paho-mqtt library docs: [https://www.eclipse.org/paho/index.php?page=clients/python/docs/index.php](https://www.eclipse.org/paho/index.php?page=clients/python/docs/index.php)  
The following tutorial might also be useful: [http://www.steves-internet-guide.com/into-mqtt-python-client/](http://www.steves-internet-guide.com/into-mqtt-python-client/)  

The created publisher should do the following:  
- Publish a random number between 0 and 10
- A number should be published once a second
- The number should be published to a public test broker, ex. mqtt.eclipse.org

**Part 4 - Creating a subscriber**  

Create a subscriber using the paho-mqtt library. The created subscriber should do the following:  
- Subscribe to the topic being published to in part 3
- Print the random value when recieved

**Part 5 - Local test**  

Test your implementation of the publisher and the subscriber.

**Part 6 - Test between team members**  

Test your implementation of the publisher and the subscriber by sending and recieving data between your implementation and the implemetation done by one of your team members.

## Exercise 3 - Research OME education

### Information

In order to have a better understanding of the people you are working with it is a good idea to research who they are as professionals.

Most of the material I could find about the OME education is in Danish, you might need to team up with danish speaking members of your team or conduct an interview with one of the OME students.

### Exercise instructions

1. In your team, research the OME education using the below questions:
  * What is the learning goals of the education ?
  * What educational level in the qualification framework is the OME education ?
  * What courses are included in the education ?
  * What kind of jobs does OME's occupy ?
  * What kind of documentation models does OME's use ?
  * What challenges can we experience in our communication with OME's
  * *Other relevant questions that you would like knowledge about*

2. Document your gained knowledge as a document in your gitlab project and link to it from the `readme.md` file

### Links

* [https://www.fms.dk/](https://www.fms.dk/)
* [https://www.ug.dk/uddannelser/professionsbacheloruddannelser/tekniskeogteknologiskeudd/maritimeuddannelser/maskinmester](https://www.ug.dk/uddannelser/professionsbacheloruddannelser/tekniskeogteknologiskeudd/maritimeuddannelser/maskinmester)
* [https://ufm.dk/en/education/recognition-and-transparency/transparency-tools/qualifications-frameworks/levels?set_language=en&cl=en](https://ufm.dk/en/education/recognition-and-transparency/transparency-tools/qualifications-frameworks/levels?set_language=en&cl=en)