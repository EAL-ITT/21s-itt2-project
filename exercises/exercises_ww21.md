---
Week: 21
tags:
- Presentations
---

# Exercises for ww21

## Exercise 1 - Presentation prep

### Information

This exercise is designed to help your team prepare your presentation for Wednesday.  
Read exercise 2 as well, before planning your presentation.

### Exercise instructions

The presentation should include the following:
1. Collaboration between the team and the OME.
2. Walktrough of the system components.
3. Walktrough of how the system is operated by a user.
4. Demonstration of the system (or working parts).

You only have 10 min. so make sure to divide accordingly between your team members.

It is a good idea to divide the presentation into segments (possibly the four points from above) and have an introductory slide in the beginning showing the overview of the presentation.

## Exercise 2 - Presentations

### Information

Since the beginning of the spring semester 2021, 8 teams of students from UCL IT Technology (ITT) have been collaborating with 3 teams of students from FMS Operationel Maintenance Engineers (OME).  
Their task has been to develop a standalone IoT sensor system that can prevent breakdowns by monitoring systems in 3 key areas in a data center: Ventilation, Cooling and Power.

At this presentation OME students will present their role in the datacenter and how they have collaborated with ITT students in developing the above mentioned systems.
ITT students will present how they collaborated with the OME students, a walkthrough of their system components as well as a demonstration of how the system is operating.

Each team of students will have 10 minutes for presentation followed by 5 minutes of feedback and questions from lecturers and other participants.

All presentations are going to be in english.   

## Formalities
**Date:** Wednesday May 26th   
**Time:** 12:30 - 15:50

### Place
[https://ucldk.zoom.us/j/66008815483?pwd=bEhFYkJWZkMwMm5WbmVjcEVONFdWZz09](https://ucldk.zoom.us/j/66008815483?pwd=bEhFYkJWZkMwMm5WbmVjcEVONFdWZz09)  

Meeting ID: 660 0881 5483   
Password:1234


### Timetable

| Time | Subject |
| :--- | :------ |
| 12:30 | Introduction
| 12:40 | OME - Ventilation
| 12:55 | ITT - Team A3
| 13:10 | ITT - Team B4
| 13:25 | Break
| 13:35 | OME - Cooling
| 13:50 | ITT - Team A1
| 14:05 | ITT - Team B1
| 14:20 | ITT - Team B3
| 14:35 | Break
| 14:50 | OME - Power
| 15:05 | ITT - Team A2
| 15:20 | ITT - Team B2
| 15:35 | ITT - Team A4
| 15:50 | End of day
