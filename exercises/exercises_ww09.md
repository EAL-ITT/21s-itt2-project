---
Week: 09
tags:
- database
- mongodb
- atlas
- api
- persistence
- cloud
---

# Exercises for ww09

## Exercise 1 - Data persistence using Mongodb

For learning purposes this is recommended as an individual exercise, for your team's system, you probably only need one database and mqtt-db microservice. 

### Information

It is good practice to persist sensor data in an IoT systems. The persisted data might be of value.  
It can be used to show history graphs, perform data analysis, extract tendencies etc.

Persisting data in IoT systems is done by using a data base. Databases comes in many flavours [https://www.javatpoint.com/types-of-databases](https://www.javatpoint.com/types-of-databases).
For large datasets a good option is the NoSQL document based databases like mongoDB. MongoDB stores data in flexible, JSON-like documents, meaning fields can vary from document to document and data structure can be changed over time
The document model maps to the objects in your application code, making data easy to work with.

In this exercise you will learn how to set up a microservice on azure, that lisens to an MQTT topic from your broker. The microservice has a connection to a cloud hosted mongoDB database on mongoDB Atlas which is the most used option when hosting mongoDB in the cloud. 

### Exercise instructions

**Part 1 - MongoDB**
1. To get an understanding of the underlying philosophy and history, read about NoSQL databases [https://www.mongodb.com/nosql-explained](https://www.mongodb.com/nosql-explained) 
2. Follow the guide on Atlas to setup and deploy a Free Tier Cluster (you need to complete the entire guide) [https://docs.atlas.mongodb.com/getting-started/](https://docs.atlas.mongodb.com/getting-started/) 

**Part 2 - From MQTT into MongoDB**  

3. Clone the mqtt-to-db example and follow the readme.md instructions in the example. Example link: [https://gitlab.com/21s-itt2-datacenter-students-group/examples/mqtt-to-db](https://gitlab.com/21s-itt2-datacenter-students-group/examples/mqtt-to-db)  
4. Publish some data to the topic which the mqtt-to-db is subscribed. You can use your own publisher or the mqtt-tests client found at [https://gitlab.com/21s-itt2-datacenter-students-group/examples/mqtt-tests](https://gitlab.com/21s-itt2-datacenter-students-group/examples/mqtt-tests) 
5. Confirm that data is saved in your mongoDB collection using the atlas dashboard
![Mongodb atlas collection view](atlas_mongo_collection.png)
6. Commit and push your working mqtt-to-db code to your gitlab project

**Part 3 - Setting it up in the cloud**  

7. Create a virtual machine for the mqtt-to-db application on Azure. Use the OS image named `Debian 10 "Buster" with backports kernel - Gen1` and a machine size named `Standard B1s (1 vcpus, 1 GiB memory)`  
Reuse your existing Azure ssh key.
8. In Azure, either setup a virtual network for your MQTT broker and mqtt-to-db virtual machines and use their corresponding private IP addresses in your mqtt-to-db application or configure a network inbound rule for the MQTT broker virtual machine to allow connections from your mqtt-db microservice
9. In Atlas, add the public IP address of the mqtt-to-db virtual machine to the IP Access List
10. Clone the mqtt-db code from your gitlab project to the virtual machine, remember to use the `-A` parameter and an ssh user agent to be able to interact with gitlab from the virtual machine.  
See this for details [https://eal-itt.gitlab.io/datacenter-iot/misc/ssh-agent-forward](https://eal-itt.gitlab.io/datacenter-iot/misc/ssh-agent-forward) 

**Part 4 - Testing the setup**

11. Start everything:
   * The MQTT broker azure microservice
   * The MQTT to database azure microservice 
   * The client on your Raspberry Pi, sending data from a sensor to the MQTT broker microservice
12. Once again, as in step 4, confirm that data is saved in your mongoDB collection on atlas
13. Document the setup with recreation steps and troubleshooting tips in a `.md` document and link to the document from your `README.md` 

*Remember to suspend virtual machines when you are not using them*

### Troubleshooting

This exercise has a lot of moving parts, having a plan and a block diagram will help you troubleshoot what is going on.

This article on the companion site can help you understand what good troubleshooting is: [https://eal-itt.gitlab.io/datacenter-iot/project_management/stuff_working](https://eal-itt.gitlab.io/datacenter-iot/project_management/stuff_working)

Below is a non complete list of things that might go wrong:

1. Make sure your broker is running
2. Make sure you are publishing to the broker from a client
3. Check inbound and outbound rules on virtual machines
4. Check allowed connections on atlas
5. Check topic names on MQTT client, broker and mqtt-db
6. Check you block diagram for clues and to keep track of where you are troubleshooting.
