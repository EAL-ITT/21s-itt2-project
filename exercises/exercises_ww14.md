---
Week: 14
tags:
- Gitlab
- Gitlab group
- Gitlab subgroup
- Group
- Subgroup
---

# Exercises for ww14

## Exercise 1 - Gitlab group

### Information

This is a team exercise.

A larger IoT project like this can be seen a set of smaller project parts. Having the solutions for the different parts in their own project is both easier to work with but also more manageable. In Gitlab we can use groups to have multiple projects grouped together under a larger project. As a simplification, a Gitlab group can be seen as a folder for your projects.  

In this project you are already working in a Gitlab group, called `21s-itt2-datacenter-students-group`. In this group each team will be given a subgroup in which they will be able to create new projects.

You can read more about Gitlab groups here: [https://docs.gitlab.com/ee/user/group/](https://docs.gitlab.com/ee/user/group/)  

You can read more about Gitlab subgroups here: [https://docs.gitlab.com/ee/user/group/subgroups/](https://docs.gitlab.com/ee/user/group/subgroups/)

### Exercise instructions

1. Make sure all the members of your team have access to your teams subgroup.
2. Make a plan for how your project can be divided into different smaller Gitlab project. A good idea is to divide it based on the programs created. Eg. mqtt-to-db is a project, your mqtt publisher is a project, etc.
3. Make your plan a reality. Create the Gitlab projects in the teams subgroup and move the related items into created the projects.
4. Create a `readme.md` in each of the projects in the teams subgroup.
5. Link to the `readme.md` in each of the projects in the teams subgroup, in the `readme.md` in the teams overall project (the one where you link to all your other assignments).


## Exercise 2 - Git branches

### Information

This exercise is an individual exercise but requires team work for planning and acceptance of merge requests.
After the completion of this exercise, it is expected that you work within git using branching, and not directly in the master branch.
Part of this exercise will be the hand in for OLA23.

GIT is using the concept of branching. All git repositories always has one branch, regardless of the platform (gitlab, github, bitbuckert etc.). That branch is called the master branch.
From the master branch you can make as many additional banches as you wish. Additional branches is usually created as a copy of the master branch and contains an exact copy of the master branch, at the point in time the new branch is created.  

When collaborating through git you will often work on an isolated part of a project.  
This can be to develop a part of application code, update documentation etc.  
In brief you are changing part of the shared files in the git repository.  

If you make changes in the same file, in the same branch (typically the master branch), as one of your teammates and you push them to the repository you will get merge conflicts. 

Merge conflicts will insert conflict markers in your files. An example is mentioned here: [https://stackoverflow.com/questions/10657315/git-merge-left-head-marks-in-my-files](https://stackoverflow.com/questions/10657315/git-merge-left-head-marks-in-my-files)

Merge conflicts has to be resolved by manually editing the file(s) that has merge conflicts. It takes time and is prone to introduce errors into the master branch.  

The solution to this is to work within another branch of the repository and resolve merge conflicts in the other branch, before merging changes into the master branch.  

When working in a branch you are essentially protecting the master branch and keeping it in a working state.
The branch you are working in does not affect the master branch until you decide to merge it back to the master branch.  
Usually this is when the work you set out to do is done and possible merge conflicts has been resolved in your branch.

To give a clearer picture about how you work with branches, consider a familiar scenario where you are a part of a team that is developing something using git as your version control system.  
The team has a few members and they divide tasks using scrum activities and the gitlab issue board.

1. When team member A picks a task from the issue board, the member begins work by assigning themselves to the issue and moving it into the `doing` column. This is mostly to communicate to the other team members that they should not pick the same task. 
2. The team member then creates a new branch, from the master branch, and gives the branch a name corresponding to the issue name and number (from the issue board). Let's call it `feature #12` branch from now on.
3. The team member pulls the `feature #12` branch with `git pull` to the local computer and swithches to the `feature #12` branch by using the git `checkout <branchname>` command
4. While working in the `feature #12` branch, the team member commits changes several times (small commits are better).
5. If the work spans several days, each day is started with pulling possible changes from the master branch, into the `feature #12` branch and resolving any conflicts in the `feature #12` branch. Not in the master branch! It has to be protected.  
The team member ends each day by pushing changes to the remote `feature #12` branch, to make sure that it is saved if something happens to the local computer.
6. Finally when work is completed in the `feature #12` the team member pulls changes from the master branch and resolves any conflicts locally. The team member the pushes to the remote `feature #12` branch and opens a pull request. It is advised to then ask another teammember to review it. If the review is passed the other team member merges the changes into the master branch. 

### Exercise instructions

**Part 1 - Knowledge on git branching**

1. Read about git branching and merging
    * [https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)
    * [https://www.atlassian.com/git/tutorials/using-branches](https://www.atlassian.com/git/tutorials/using-branches)
2. Read about branching strategies
    * Git flow [https://datasift.github.io/gitflow/IntroducingGitFlow.html](https://datasift.github.io/gitflow/IntroducingGitFlow.html)
    * Maybe Git flow is too complex ? [https://about.gitlab.com/blog/2020/03/05/what-is-gitlab-flow/](https://about.gitlab.com/blog/2020/03/05/what-is-gitlab-flow/)
    * Git lab flow (the simple and recommended strategy) [https://git.metabarcoding.org/help/workflow/gitlab_flow.md](https://git.metabarcoding.org/help/workflow/gitlab_flow.md)
3. Read about linking issues to branches
    * [https://git.metabarcoding.org/help/workflow/gitlab_flow.md#issue-tracking-with-gitlab-flow](https://git.metabarcoding.org/help/workflow/gitlab_flow.md#issue-tracking-with-gitlab-flow)
    * [https://stackoverflow.com/questions/43295151/creating-a-branch-from-issue-in-gitlab](https://stackoverflow.com/questions/43295151/creating-a-branch-from-issue-in-gitlab)
4. Decide on a branching strategy in the team
    * Document it on gitlab and link to it from your `readme.md`

**Part 2 - Practice branching**

While working take notes of links, screenshots and relevant GIT commands for working with branches (new branch, pull branch, checkout branch, pull from master, merge request etc.).

1. Pick an issue from your issue board
2. Create a branch and pull it to your computer
3. Make changes and several commits during work (remember to switch branch)
4. Pull from master branch to your branch `git pull origin master`
5. Resolve any merge conflicts in your favourite text editor
6. Push to the remote repository
7. Create a merge request (remember to link to the issue in the description) and ask a team member to review and merge it (use gitlabs functionality for this)
8. Examine the branch graph in your gitlab project. Example here: [https://gitlab.com/EAL-ITT/21s-itt2-project/-/network/master](https://gitlab.com/EAL-ITT/21s-itt2-project/-/network/master)
9. Use branches for everything (almost) in the future.

**Additional information**

This exercise is not a step by step guide and requires research, curiosity and persistence from you.  
Completing this exercise is the foundation for working with OLA23.  
If you get stuck you are, as always, welcome to ask for help.

* Official GIT documentation [https://git-scm.com/doc](https://git-scm.com/doc)
* Gitlab GIT CLI documentation [https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)