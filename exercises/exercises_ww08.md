---
Week: 08
tags:
- mqtt broker
- scrum master
- facilitator
- secretary
- milestones
- cloud
---

# Exercises for ww08

## Exercise 1 - MQTT broker as a microservice on Azure

### Information

For learning purposes this is recommended as an individual exercise, for your team's system, you probably only need one MQTT broker. 

For the previous MQTT exercises you have been using a public and free test MQTT broker. In production this is not adviced for more than one reason. Privacy, availability, configurability etc.

This exercise teaches you how to set up your own MQTT broker, as a microservice, in a virtual machine running on Microsoft Azure Cloud.
You as a UCL student already have an account on azure for education, it is tied to your UCL credentials.

The broker is built using Mosquitto running on Linux OS - Debian 10 Buster

The definition of microservices are not set in stone, in the context of this project consider microservices, as services running on seperate virtual machines in the cloud, that communicate with each other using different protocols.

### Exercise instructions

**Part 1 - learn about microservices and access Azure student credits**

1. Read about microservices and service oriented architecture (SOA) [https://opensource.com/resources/what-are-microservices](https://opensource.com/resources/what-are-microservices)
2. Activate your free credits on azure (click activate now) [https://azure.microsoft.com/en-us/free/students/](https://azure.microsoft.com/en-us/free/students/)
3. log in using your UCL credentials (this might require a 2FA verification process via your phone)

**Part 2 - Setup a virtual machine**

For this part use the OS image named `Debian 10 "Buster" with backports kernel - Gen1` and a machine size named `Standard B1s (1 vcpus, 1 GiB memory)`

1. Follow the guide at [https://docs.microsoft.com/en-us/azure/virtual-machines/linux/quick-create-portal](https://docs.microsoft.com/en-us/azure/virtual-machines/linux/quick-create-portal)
2. Do not setup a web server, stop when you have connected to the virtual machine (VM) using SSH
3. In the Azure dashboard go to your virtual machine and click `network` on the left sidebar menu
4. Add inbound and outbound rules for port 1883 (you might want to restrict these rules to your own public ip for security reasons)
5. On the left sidebar click overview and assign a DNS name to the virtual machine

**Part 3- Setup Mosquitto**

1. If not already logged in, log in to the virtual machine.
2. follow the guide at: [https://medium.com/@rossdanderson/installing-mosquitto-broker-on-debian-2a341fe88981](https://medium.com/@rossdanderson/installing-mosquitto-broker-on-debian-2a341fe88981)    
Stop when reaching the part *Adding Websocket protocol support*

**Part 4 - Test the broker with you MQTT application**

1. Modify the MQTT code you have from exercise 4 in week 7 to use your own broker
2. Test that you can publish and subscribe to MQTT topics using the broker
3. Push the modified code to your gitlab project and update relevant documentation ie. broker DNS name etc.

**Part 5 - Suspend virtual machine**

To prevent your VM from consuming money when you are not using it you should suspend it.

1. In the Azure dashboard go to your virtual machine and click `Stop` 
2. When asked if you would lilke to reserve the public IP, answer yes.
3. Confirm that the VM status is `Stopped (deallocated)`
 

## Exercise 2 - Scrum master, facilitator, secretary on rotation

### Information
This exercise will help you plan the rotation of the Scrum master, facilitator and secretary role. It could be a good idea to have the Scrum master and the facilitator be the same person for a given sprint, since they have overlapping responsibilities, but it is up to you to decide and reason your choices.  

The following link can be used as inspiration regarding the facilitator, look at the team facilitator headline: [https://eal-itt.gitlab.io/collaboration-days/why_teams/](https://eal-itt.gitlab.io/collaboration-days/why_teams/)  

The following link can be used as inspiration regarding the Scrum master: [https://www.scrum.org/resources/what-is-a-scrum-master](https://www.scrum.org/resources/what-is-a-scrum-master)  

### Exercise instructions
1. Create a document
2. Make a detailed description of the responsibilities and tasks of the Scrum master, facilitor and secretary in your project.
3. Make a plan detailing who has been and is going to be the Scrum master, facilitator and secretary for each sprint. (Everyone should have been the Scrum master, facilitator and secretary at least once at the end of the project)
4. Include the document in your gitlab project and link to the document from your `README.md`.


## Exercise 3 - Milestones using gitlab

### Information

In the project lecture plan 3 milestones have been defined.

* Week 10: Proof of concept (POC)
* Week 15: Minimum viable product (MVP)
* Week 20: Final adjustments and prepare presentations

The actual content of these milestones, in regards to, tasks and issues are defined by you through your scrum activities.
Gitlab has functionality to define milestones and associate gitlab issues to these milestones.

You can also think of each sprint as a milestone and you are free to create milestones for each sprint if you think it makes sense in your team. Gitlab have another tool that is called iterations that might be more suitable for managing sprint called iterations [https://about.gitlab.com/handbook/marketing/project-management-guidelines/milestones/#iterations](https://about.gitlab.com/handbook/marketing/project-management-guidelines/milestones/#iterations)

The milestones are used to monitor progress towards a milestone, the tool for monitoring is called a burndown chart: [https://en.wikipedia.org/wiki/Burn_down_chart](https://en.wikipedia.org/wiki/Burn_down_chart)

This exercise will teach you about milestones and how to manage them with gitlab.

### Exercise instructions

1. Read about milestones guidelines: [https://about.gitlab.com/handbook/marketing/project-management-guidelines/milestones/](https://about.gitlab.com/handbook/marketing/project-management-guidelines/milestones/)
2. Read about milestone usage [https://docs.gitlab.com/ee/user/project/milestones/](https://docs.gitlab.com/ee/user/project/milestones/)
2. Create milestones in your gitlab project
3. Set start and end dates for each milestone
4. Describe the goals of the milestone in the milestone description 
5. Assign issues to milestones (you can also assign closed issues to milestones)  This is of course a continous thing that you will have to remember to use when you create new issues
6. Examine each milestone burndown chart
6. Link to your milestone in the `README.md` file 