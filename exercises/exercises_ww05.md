---
Week: 05
tags:
- pre mortem
- project plan
- gitlab
- scrum
---

# Exercises for ww05

## Exercise 1 - Gitlab project access

### Information

Your part (IT technology teams) of the project needs to be documented in a Gitlab project.  
The project is already created but your team needs appropiate access level (currently guest)

### Exercise instructions

Write your team's gitlab usernames in the form `@username` to NISI or MEGR for access to the project. We need a complete list of the team, not individual usernames. 

If you need a recap on working with git and gitlab, follow the Gitlab daily workflow codelab [https://npes.gitlab.io/claat-generator/gitlab_daily_workflow/index.html#0](https://npes.gitlab.io/claat-generator/gitlab_daily_workflow/index.html#0)


## Exercise 2 - Start using Scrum

### Information
This exercise will help your team getting started using Scrum. It should be noted that the meetings in Scrum have a set duration. It is important that you try to keep the meeting within this lenght, as this restriction is where a lot of the benefits of Scrum comes from.  
Materials related to the specific Scrum artefacts and meeting types will be supplied as they are needed in the exercises. This material should be read before starting the exercise.  

You can read more about Scrum here: [https://www.scrum.org/resources/what-is-scrum](https://www.scrum.org/resources/what-is-scrum)  

You can also download The Scrum Guide pdf here: [https://www.scrumguides.org/docs/scrumguide/v2020/2020-Scrum-Guide-US.pdf](https://www.scrumguides.org/docs/scrumguide/v2020/2020-Scrum-Guide-US.pdf)  

### Exercise instructions

**Part 1 - Product backlog**  
Resource to read before starting: [Product backlog](https://www.scrum.org/resources/what-is-a-product-backlog)  

Create an initial product backlog for your project. This should be done using a board on your Gitlab.  
The tasks added to your product backlog should not take longer to complete than the duration of a sprint, which for you is 2 weeks.

Since you are just starting your project there are a lot of tasks that you do not know of yet and, by extension, can not add to product backlog. However, there are still the project setup and initial exercises which are excellent tasks to start with. The product backlog is dynamic, meaning that as you progress with your project, more tasks will be added to the product backlog.

**Part 2 - The first sprint planning meeting (45 min)**  
Resource to read before starting: [Sprint planning](https://www.scrum.org/resources/what-is-sprint-planning)  

Go through the teams first sprint planning session. Have someone keep track of time, making sure you try to limit yourself to the 45 min. lenght restriction.  
Discuss the following topics:
1. Why is this sprint valuable?
2. What can be done in this sprint? (This is where you select tasks from your product backlog that should be worked on in the sprint)
3. How will the chosen work get done?

**Part 3 - The first sprint backlog**  

Resource to read before starting: [Sprint backlog](https://www.scrum.org/resources/what-is-a-sprint-backlog)  

Create a sprint backlog based on the discussed topics in the sprint planning meeting. This should also be done using a board on your Gitlab. The sprint backlog should look like the one on the model from the datacenter companion site -> [Datacenter companion site - Scrum](https://eal-itt.gitlab.io/datacenter-iot/project_management/scrum)  

**Part 4 - Start working**  

Start working on the tasks in the sprint backlog. Part 4 is meant as a way to end the exercise while pushing you in the next direction of using Scrum. You are not supposed to complete all your tasks, just start working with them. The sprint backlog should be updated as you work with the tasks.

## Exercise 3 - The first daily scrum meeting (15 min)

### Information
Resource to read before starting: [Daily scrum meeting](https://www.scrum.org/resources/what-is-a-daily-scrum)  
This exercise will help your team through your first daily scrum meeting. The daily scrum meeting is only 15 min. A person acting as the scrum master will keep track of the time and make sure everyone gets to speak. If a subject needs to be discussed for a longer period of time, plan a meeting for discussing the subject during the daily scrum instead of using most the time discussing only this subject. 

### Exercise instructions
1. Select a person to act as the meetings scrum master
2. Have the daily scrum meeting

## Exercise 4 - The end of the first sprint loop

### Information
This exercise will help your team through your first ending of a sprint loop.

### Exercise instructions

**Part 1 - The first sprint review meeting (45 min)**  
Resource to read before starting: [Sprint review](https://www.scrum.org/resources/what-is-a-sprint-review)  

Go through the teams first sprint review session. Have someone keep track of time, making sure you try to limit yourself to the 45 min. lenght restriction.  
Discuss the following topics:
1. What have been done and what has not been done?
2. What went well in the sprint and were there any problems?
3. Each member presents the work they did and answer questions
4. What to do next?
5. Review of potential changes to the project

**Part 2 - The first sprint retrospective meeting (20 min)**  

Resource to read before starting: [Sprint retrospective](https://www.scrum.org/resources/what-is-a-sprint-retrospective)  

Go through the teams first sprint retrospective session. Have someone keep track of time, making sure you try to limit yourself to the 20 min. lenght restriction.  
Discuss the following topics:
1. What went well in the sprint?
2. What could be improved?
3. What will we commit to improve in the next sprint?

**Part 3 - The second sprint planning meeting (45 min)** 

Resource to read before starting: [Sprint planning](https://www.scrum.org/resources/what-is-sprint-planning)  

Go through the teams second sprint planning session. Have someone keep track of time, making sure you try to limit yourself to the 45 min. lenght restriction.  
Discuss the following topics:
1. Why is this sprint valuable?
2. What can be done in this sprint?
3. How will the chosen work get done?


**Part 4 - The second sprint backlog**  

Resource to read before starting: [Sprint backlog](https://www.scrum.org/resources/what-is-a-sprint-backlog)  

Modify your sprint backlog on gitlab based on the discussed topics in the sprint planning meeting.

**Part 5 - Start working**  

Start working on the tasks in the sprint backlog. Part 5 is meant as a way to end the exercise while pushing you in the next direction of using Scrum. You are not supposed to complete all your tasks, just start working with them. The sprint backlog should be updated as you work with the tasks.

## Exercise 5 - pre mortem

### Information

Imagine that the project has failed and ask, "what did go wrong" ?  
The team members’ task is to generate plausible reasons for the project’s failure.

### Exercise instructions

**Part 1**

1. Individually read the article on performing a pre mortem [https://hbr.org/2007/09/performing-a-project-premortem](https://hbr.org/2007/09/performing-a-project-premortem)

In your team use the cooperative learning structure **think-pair-share** [https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf#section.5](https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf#section.5) to find out what went wrong ?

2. Think (10 minutes)  
    Write down a numbered list containing every reason you can think of for the failure. Including things you ordinarily wouldn’t mention as potential problems, for fear of being impolitic.
3. Pair (5 minutes)  
    Compare your lists and remove possible duplicates
4. Share (10 minutes)  
    read one reason from your pair list and explain the reason for putting it on the list. Continue until all reasons have been recorded in a team list and order the list by probablilty.

**Part 2**  

Include the ordered and numbered list in your gitlab project and share a link to the project plan in the readme.md file

## Exercise 6 - Update project plan

### Information

We have supplied a generic project plan, but you still have to fill in the student parts.  
This is considered the first iteration of the project plan, you might need to update it at a later stage in the project.  

The project plan can be downloaded at: [https://gitlab.com/EAL-ITT/21s-itt2-project/-/raw/master/docs/no_render/project_plan_for_students.md?inline=false](https://gitlab.com/EAL-ITT/21s-itt2-project/-/raw/master/docs/no_render/project_plan_for_students.md?inline=false)

### Exercise instructions

1. Include the [project plan] in your gitlab project
2. Fill in relevant parts marked: **TBD: This section must be completed by the students.**
3. Create or update the readme.md file in your gitlab project with team members first and last names. Use this format: `firstname lastname`
4. Link to the project plan in the readme.md file

Remember to rename the project plan to include your team name