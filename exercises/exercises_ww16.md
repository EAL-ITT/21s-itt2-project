---
Week: 16
tags:
- MVP
- MVP video
- 
---

# Exercises for ww16

## Exercise 1 - MVP video

### Information

This is a team exercise.

Minimum Viable Product (MVP) ended in week 15 and we would like to see what you have accomplished.

### Exercise instructions

You need to produce a video ~3 minutes long, where the target audience is the customer (OMEs). The video should atleast include the following:

1. An introduction of the MVP and its differrent parts.
2. A showcase of how to use the MVP.
3. A deeper look into how the different parts of the MVP work.

Remember the target audience does not have the same technical knowledge as you, so think about your choice of words and use of industry terms. Remember also to use the feedback you got for your POC video.


The video should be uploaded to youtube or similar and shared with teachers and the other project groups. Remember to include a link to the video in your projects `readme.md`.


## Exercise 2 - MVP user test

### Information

This is a team exercise.  

Testing your development results against actual users, is the preferred way of confirming that you are in sync with your users expectations.
User testing has a long history and is used in almost every line of business, from food products to web sites to electronic products.

Testing with users can be done in any stage of development and it is often a good idea to start testing before development.  
But how? you might say. By using mockups I reply.  

A mockup is a representation of your product that has no actual functionality, but in some way describes the functionality enough for your users to give you feedback.  
If you are developing a hardware device and need feedback on the user interface you can build a mockup from cardboard.  
If you are developing a dashboard for IoT you can make a mockup that shows the elements (graphs, gauges, input fields, buttons) and dummy data, without connecting it to the actual IoT sensor infrastucture.  

At this point in the project you should a MVP, built from your users requirements (the OME's). If not, use a mockup for this exercise.  

The purpose of this exercise is to learn about user testing methods as well as setting up and completing a user test of your MVP.


### Exercise instructions  

**To begin, prepare a .md document to document your user testing process.**  
**While working on the exercise note every decision, argument and other relevant stuff from the process in that document.**  
**Link to the document from your `readme.md` file**

**Part 1 - Research user testing methods**

Read about different user testing methods, while reading think how the specific method applies to your MVP and what kind of feedback it can give you from your users.

Below are suggestions to get you on the right path for researching different testing methods.  
Your research should not be limited to the suggestions.

* [https://www.yukti.io/10-most-important-user-testing-methods/](https://www.yukti.io/10-most-important-user-testing-methods/)  
Remember that it has links to indepth material of the different methods
* [https://www.fictiv.com/articles/5-essential-tips-for-user-testing-your-hardware-project](https://www.fictiv.com/articles/5-essential-tips-for-user-testing-your-hardware-project)
* [https://userpeek.com/blog/user-experience-testing/](https://userpeek.com/blog/user-experience-testing/)


**Part 2 - Design and prepare the user test**

1. In your team decide which user testing method(s) you are going to use.
2. Decide how you are giong to observe and document your observations of the user tests. 
3. Decide on how you are going to evaluate the test results?  
If you are using a video recording it will take a long time to evaluate the results, if you are using a guide with questions it might be quick to evaluate results. Think about this when deciding.
4. Ask your users to participate in the test and arrange a fixed date and time.

**Part 3 - Evaluate**


1. Evaluate your user tests according to plan from part 2.
2. Decide if and how it changes your MVP
3. Update design documentation according to found changes
4. Plan work for implemementation of changes using the issue board.