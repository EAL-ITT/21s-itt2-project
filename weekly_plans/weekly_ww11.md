---
Week: 11
Content: Minimum viable product
Material: See links in weekly plan
Initials: NISI/MEGR
---

# Week 11

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* An updated and ready to use issue board


### Learning goals

* Minimum viable product
  * Level 1: The student knows the purpose of a minimum viable product
  * Level 2: The student can plan for implementation of a minimum viable product
  * Level 3: The student can plan for user testing of a minimum viable product


## Deliverables
* Minimum viable product milestone created on Gitlab

## Schedule

### Monday

Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:30 | Daily scrum - 15 minutes|
| 9:45 | Hands-on time |
| 12:30 | Q&A Session |
| 13:00 | Lunch | 
| 13:45 | Hands-on time |
| 16:15 | End of day |

### Wednesday

This day will include the Virtual Career & Internship Day hosted by the Danish Data Center Industry.

| Time | Activity |
| :---: | :--- |
| 9:00 | Daily scrum - 15 minutes|
| 9:15 | Hands-on time |
| 12:30 | Virtual Career & Internship Day - Danish Data Center Industry |
| 15:30 | Hands-on time |
| 16:15 | End of day |

## Hands-on time

See the exercise page for details [https://eal-itt.gitlab.io/21s-itt2-project/exercises/](https://eal-itt.gitlab.io/21s-itt2-project/exercises/)

### Exercise 1 - Catch/clean up

...

### Exercise 2 - Minimum Viable Product

...

## Comments

* Make sure that you are signed up for the Virtual Career & Internship Day.  
  Signup link [https://datacenterindustrien.dk/focusareas/virtualday/](https://datacenterindustrien.dk/focusareas/virtualday/)
