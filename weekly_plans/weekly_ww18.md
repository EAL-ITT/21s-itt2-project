---
Week: 18
Content: Consolidation
Material: See links in weekly plan
Initials: NISI/MEGR
---

# Week 18

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* ...


### Learning goals
* Custom RPi images
  * Level 1: The student knows the benefits of building images
  * Level 2: The student can set up build of a simple image using doLevel 1: cker, scripts and gitlab pipeline
  * Level 3: The student can set up an advanced image using git, docker, scripts and gitlab pipeline

* Business understanding
  * Level 1: The student knows of different business types in denmark
  * Level 2: The student can read an organisational chart
  * Level 3: The student can evaluate an organisational structure based on the product / service provided by the organisation

## Deliverables
* Repository with custom RPi image
* Business documents from exercise 2

## Schedule

Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

### Monday

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:15 | Custom raspberry images - Walkthrough |
| 10:15 | Daily scrum - 15 minutes|
| 10:30 | Hands-on time |
| 12:30 | Q&A Session |
| 13:00 | Lunch | 
| 13:45 | Hands-on time |
| 16:15 | End of day |

### Wednesday

| Time | Activity |
| :---: | :--- |
| 9:15 | Introduction |
| 9:30 | Technology Denmark [https://future-skills.io/](https://future-skills.io/) |
| 9:45 | Daily scrum - 15 minutes|
| 10:00 | Hands-on time |
| 12:30 | OME meetings |
| 13:00 | Lunch (agree with the OME's) | 
| 15:00 | Hands-on time |
| 16:15 | End of day |

## Hands-on time

See the exercise page for details [https://eal-itt.gitlab.io/21s-itt2-project/exercises/](https://eal-itt.gitlab.io/21s-itt2-project/exercises/)

### Exercise 1 - Custom RPi images

...

### Exercise 2 - Business understanding

...

## Comments

* None at this time
