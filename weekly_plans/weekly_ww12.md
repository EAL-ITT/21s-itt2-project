---
Week: 12
Content: Minimum viable product
Material: See links in weekly plan
Initials: NISI/MEGR
---

# Week 12

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Sensor(s) ordered
* Meeting with OME's

### Learning goals

* Node-red `function` node
  * Level 1: The student can insert and setup a `function` node in node-red
  * Level 2: The student can output a msg from a `function` node
  * Level 3: The student can output a msg based on incoming an incoming msg from a `function` node

* Javascript
  * Level 1: The student knows the generel syntax of Javascript
  * Level 2: The student can write Javascript functionality in node-red
  * Level 3: The student can manipulate Javascript objects in node-red

## Deliverables
* MVP milestone updated according to exercise 1
* Sensor(s) ordered according to exercise 3

## Schedule

Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

### Monday

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:30 | Daily scrum - 15 minutes|
| 9:45 | Hands-on time |
| 12:30 | Q&A Session |
| 13:00 | Lunch | 
| 13:45 | Hands-on time |
| 16:15 | End of day |

### Wednesday

This morning is without lecturer. We will join you on Zoom at 12:00.

| Time | Activity |
| :---: | :--- |
| 9:00 | Daily scrum - 15 minutes|
| 9:15 | Hands-on time |
| 12:00 | Q&A session on Zoom |
| 12:15 | Evaluation with Helle Thastum on zoom |
| 12:45 | OME meetings |
| 13:00 | Lunch (agree with the OME's) | 
| 15:00 | Hands-on time |
| 16:15 | End of day |

## Hands-on time

See the exercise page for details [https://eal-itt.gitlab.io/21s-itt2-project/exercises/](https://eal-itt.gitlab.io/21s-itt2-project/exercises/)

### Exercise 1 - Requirements agreed and decided

...

### Exercise 2 - Node-red `function` node and Javascript

...

### Exercise 3 - Sensor purchase

...

## Comments

* None at this time
