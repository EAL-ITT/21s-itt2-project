---
Week: 16
Content: Consolidation
Material: See links in weekly plan
Initials: NISI/MEGR
---

# Week 16

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* MVP video created and shared
* MVP user test planned and performed


### Learning goals

* MVP video
  * Level 1: The student can make a plan on how to showcase a product through a video format
  * Level 2: The student can showcase a product through a video format
  * Level 3: The student can take the viewer/customer into account when showcasing a product

* MVP user test
  * Level 1: The student knows different types of user tests
  * Level 2: The student can plan a user test
  * Level 3: The student can use a user test to validate and plan changes in a development process.  


## Deliverables
* MVP Video
* User test documentation

## Schedule

All presentations will be online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

### Monday

B class are allowed to work from school physically, rooms are announced on time edit.

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:30 | Daily scrum - 15 minutes|
| 9:45 | Hands-on time |
| 12:30 | Q&A Session |
| 13:00 | Lunch | 
| 13:45 | Hands-on time |
| 16:15 | End of day |

### Wednesday

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:30 | Daily scrum - 15 minutes|
| 9:45 | Hands-on time |
| 12:30 | OME meetings |
| 13:00 | Lunch (agree with the OME's) | 
| 15:00 | Hands-on time |
| 16:15 | End of day |

## Hands-on time

See the exercise page for details [https://eal-itt.gitlab.io/21s-itt2-project/exercises/](https://eal-itt.gitlab.io/21s-itt2-project/exercises/)

### Exercise 1 - MVP video

...

### Exercise 2 - MVP user test

...

## Comments

* OLA21 2nd attempt - handin on wiseflow 
