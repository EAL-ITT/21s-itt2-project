---
Week: 09
Content: Proof of concept
Material: See links in weekly plan
Initials: NISI/MEGR
---

# Week 09

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Complete exercises
* Participate in scrum activities
* Participate in OME meeting

### Learning goals

* Data persistence using MongoDB
  * Level 1: The student knows what a NoSQL database is
  * Level 2: The student can set up a MQTT database cloud microservice
  * Level 3: The student can test, troubleshoot and document microservice infrastructure


## Deliverables
* Output from exercise 1 (recreation and code documentation)

## Schedule

This day is without lecturers, please ask questions on element.

### Monday

| Time | Activity |
| :---: | :--- |
| 9:00 | Daily scrum - 15 minutes|
| 9:45 | Hands-on time |
| 13:00 | Lunch | 
| 13:45 | Hands-on time |
| 16:15 | End of day |

### Wednesday

This day is without lecturers, please ask questions on element.

| Time | Activity |
| :---: | :--- |
| 9:00 | Daily scrum - 15 minutes|
| 9:45 | Hands-on time |
| 12:30 | OME meetings |
| 13:00 | Lunch (agree with the OME's) | 
| 15:00 | Hands-on time |
| 16:15 | End of day |

## Hands-on time

See the exercise page for details [https://eal-itt.gitlab.io/21s-itt2-project/exercises/](https://eal-itt.gitlab.io/21s-itt2-project/exercises/)

### Exercise 1 - Data persistence using Mongodb

...

## Comments

* None at this moment
