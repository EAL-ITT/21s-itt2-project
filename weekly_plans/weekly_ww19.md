---
Week: 19
Content: Consolidation
Material: See links in weekly plan
Initials: NISI/MEGR
---

# Week 19

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Participate in application/CV workshop


### Learning goals

* Project website
  * Level 1: The student can set up a static website
  * Level 2: The student can organise information 
  * Level 3: The student can communicate key results

* Signal processing
  * Level 1: The student can implement a mean operation on a list values
  * Level 2: The student can implement a moving avarage / boxcar flter
  * Level 3: The student can utilise a moving average / boxcar filter to filter sensor data


## Deliverables
* Project website completed with specified content

## Schedule

Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

### Monday

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:30 | Daily scrum - 15 minutes|
| 9:45 | Hands-on time |
| 12:30 | Q&A Session |
| 13:00 | Lunch | 
| 13:45 | Hands-on time |
| 16:15 | End of day |

### Wednesday

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:30 | Daily scrum - 15 minutes|
| 10:00 | HK application/CV workshop |
| 11:30 | Hands-on time |
| 12:30 | OME meetings |
| 13:00 | Lunch (agree with the OME's) | 
| 15:00 | Hands-on time |
| 16:15 | End of day |

## Hands-on time

See the exercise page for details [https://eal-itt.gitlab.io/21s-itt2-project/exercises/](https://eal-itt.gitlab.io/21s-itt2-project/exercises/)

### Exercise 1 - Project website

...

### Exercise 2 - Signal processing

...

## Comments

* None at this time
