---
Week: 06
Content: Proof of concept
Material: See links in weekly plan
Initials: NISI/MEGR
---

# Week 06

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Participation in kickoff day
* Complete exercises
* Identify and describe team tasks (Curious and pro active approach)

### Learning goals

* System brainstorm, analysis and requirements draft
  * Level 1: The student knows methods to create a system requirement fraft
  * Level 2: The student can, in a team setting, create relevant system documentation
  * Level 3: The student can, in a team setting, plan work as s.m.a.r.t tasks, from produced requirements 

* MQTT
  * Level 1: The student knows the basic structure of a MQTT system
  * Level 2: The student can implement a basic MQTT publisher and subscriber 
  * Level 3: The student can use features like: topic trees, QoS and retain

* Research OME education
  * Level 1: The student knows what professional areas an OME works within
  * Level 2: The student can distinguish the differences between the IT Technology and OME education
  * Level 3: The student can predict examples of communication challenges with the OME's

## Deliverables
* Output from kickoff day
* Links to exercise output in `readme.md` 

## Teams and subjects

**Below are the subjects that each team is assigned to:**

| Subject | Team |
| :--- | :--- |
| Cooling | A1 |
| Cooling | B1 |
| Cooling | B3 |
| Electricity | A2 |
| Electricity | A4 |
| Electricity | B2 |
| Ventilation | A3 |
| Ventilation | B4 |

## Schedule

### Monday - Kickoff with DDI and OME

Online at zoom: [https://ucldk.zoom.us/j/63236122751](https://ucldk.zoom.us/j/63236122751)   
*Please have your camera turned on and your microphone turned off*  

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction to the day (Lars Bojen - project responsible) |
| 9:20 | What is a datacenter? (Arnth B. Nielsen, DataCenterGruppen)|
| 10:00 | Break |
| 10:15 | Introduction to Power, Cooling and ventilation in a datacenter (Martin V. Jeppesen, NNIT) |
| 11:00 | Introduction to project work (Mathias Elbæk Gregersen) | 
| 11:30 | Teamwork OME+IT Technology students (KPI's, Teamcontract, google doc desicion summary) |
| 15:00 | End of day |

### Wednesday

Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*  

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:30 | Daily scrum - 15 minutes|
| 9:45 | Hands-on time |
| 12:30 | Presentations and discussion |
| 13:00 | Lunch | 
| 13:45 | Hands-on time |
| 16:15 | End of day |

## Hands-on time

See the exercise page for details [https://eal-itt.gitlab.io/21s-itt2-project/exercises/](https://eal-itt.gitlab.io/21s-itt2-project/exercises/)

### Exercise 1 - System brainstorm, analysis and requirements draft

...

### Exercise 2 - MQTT introduction

...

### Exercise 3 - Research OME education

...

## Comments

* None at this moment
