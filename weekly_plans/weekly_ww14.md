---
Week: 14
Content: Minimum viable product
Material: See links in weekly plan
Initials: NISI/MEGR
---

# Week 14

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Project structured into the teams Gitlab group


### Learning goals

* Gitlab groups
  * Level 1: The student knows of Gitlab groups
  * Level 2: The student can create new projects in a Gitlab group
  * Level 3: The student can struture a project using a Gitlab group

* Git branches
  * Level 1: The student has knowledge about branches in GIT
  * Level 2: The student can create and commit to seperate branches in a GIT repository
  * Level 3: The student can select a branching strategy and implement it in a project context


## Deliverables
* OLA23 1st attempt

## Schedule

### Monday

No lectures because of Easter Monday

### Wednesday

Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:30 | Daily scrum - 15 minutes|
| 9:45 | Hands-on time |
| 12:30 | OME meetings |
| 13:00 | Lunch (agree with the OME's) | 
| 15:00 | Hands-on time |
| 16:15 | End of day |

## Hands-on time

See the exercise page for details [https://eal-itt.gitlab.io/21s-itt2-project/exercises/](https://eal-itt.gitlab.io/21s-itt2-project/exercises/)

### Exercise 1 - Gitlab group

...

### Exercise 2 - Git branches (OLA23)

...

## Comments

* OLA23 1st attempt - handin on wiseflow 
