---
Week: 10
Content: Proof of concept
Material: See links in weekly plan
Initials: NISI/MEGR
---

# Week 10

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* A simple node-red application
* POC video created and shared

### Learning goals

* Node-RED
  * Level 1: The student can setup a node-red application on a VM
  * Level 2: The student can create node-red applications
  * Level 3: The student can use node-red in conjunction with other technologies like mqtt and MongoDB

* POC milestone video presentation
  * Level 1: The student can communicate results
  * Level 2: The student can make a plan on how to communicate key results
  * Level 3: The student can communicate key results


## Deliverables
* POC video link in the `README.md` file

## Schedule

### Monday

Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:30 | Daily scrum - 15 minutes|
| 9:45 | Hands-on time |
| 12:30 | Q&A Session |
| 13:00 | Lunch | 
| 13:45 | Hands-on time |
| 16:15 | End of day |

### Wednesday

This day is without lecturers, please ask questions on element.

| Time | Activity |
| :---: | :--- |
| 9:00 | Daily scrum - 15 minutes|
| 9:15 | Hands-on time |
| 12:30 | OME meetings |
| 13:00 | Lunch (agree with the OME's) | 
| 15:00 | Hands-on time |
| 16:15 | End of day |

## Hands-on time

See the exercise page for details [https://eal-itt.gitlab.io/21s-itt2-project/exercises/](https://eal-itt.gitlab.io/21s-itt2-project/exercises/)

### Exercise 1 - node-red dashboard on Azure

...

### Exercise 2 - POC milestone video presentation

...

## Comments

* None at this moment
